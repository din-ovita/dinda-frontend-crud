// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDoZnf7Wb8IrDCjoDIJt9u2X_OODTPXOic",
  authDomain: "upload-image-product.firebaseapp.com",
  projectId: "upload-image-product",
  storageBucket: "upload-image-product.appspot.com",
  messagingSenderId: "400187334546",
  appId: "1:400187334546:web:bf6d5902c7dc09a258eb4b",
  measurementId: "G-QQ9PXT7DWD"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);

export default firebase;
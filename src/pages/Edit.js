import axios from "axios";
import React, { useEffect, useState } from "react";
import { Form, InputGroup } from "react-bootstrap";
import { useHistory, useParams } from "react-router-dom";
import Swal from "sweetalert2";
import "../style/edit.css";

export default function Edit() {
  const param = useParams();
  const [img, setImg] = useState(null);
  const [nama, setNama] = useState("");
  const [deskripsi, setDeskripsi] = useState("");
  const [harga, setHarga] = useState(0);

  const history = useHistory();

  const updateList = async (e) => {
    e.persist();
    e.preventDefault();

    const fromData = new FormData();
    fromData.append("file", img);
    fromData.append("nama", nama);
    fromData.append("deskripsi", deskripsi);
    fromData.append("harga", harga);

    Swal.fire({
      title: "Apakah Ingin Di Update?",
      text: "Data kamu yang lama tidak bisa dikembalikan!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, Di Update!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .put("http://localhost:3009/produk/" + param.id, fromData)
          // headers berikut berfungsi untuk method yang akan diakses oleh admin
          //   {
          //     headers: {
          //       "Content-Type": "multipart/form-data",
          //       Authorization: `Bearer ${localStorage.getItem("token")}`,
          //     },
          //   }
          // )
          .then(() => {
            history.push("/homeAdmin");
            Swal.fire("Berhasil", "Data kamu berhasil di update", "success");
          })
          .catch((error) => {
            console.log(error);
          });
      }
    });
  };

  useEffect(() => {
    // library opensource yang digunakan untuk request data melalui http
    axios
      .get("http://localhost:3009/produk/" + param.id)
      .then((response) => {
        const newMenu = response.data.data;
        //const newMenu = response.data;
        setImg(newMenu.img);
        setNama(newMenu.nama);
        setDeskripsi(newMenu.deskripsi);
        setHarga(newMenu.harga);
      })
      .catch((error) => {
        alert("Terjadi kesalahan Sir!" + error);
      });
  }, []);

  // const updateList = async (e) => {
  //   e.preventDefault();

  //   Swal.fire({
  //     title: "Apakah Ingin Di Update?",
  //     text: "Data kamu yang lama tidak bisa dikembalikan!",
  //     icon: "warning",
  //     showCancelButton: true,
  //     confirmButtonColor: "#3085d6",
  //     cancelButtonColor: "#d33",
  //     confirmButtonText: "Ya, Di Update!",
  //   }).then((result) => {
  //     if (result.isConfirmed) {
  //       axios
  //         .put(
  //           "http://localhost:3009/produk/" + param.id,
  //           {
  //             nama: nama,
  //             deskripsi: deskripsi,
  //             harga: Number(harga),
  //           },
  //           // headers berikut berfungsi untuk method yang akan diakses oleh admin
  //           {
  //             headers: {
  //               Authorization: `Bearer ${localStorage.getItem("token")}`,
  //             },
  //           }
  //         )
  //         .then(() => {
  //           history.push("/homeAdmin");
  //           Swal.fire("Berhasil", "Data kamu berhasil di update", "success");
  //         })
  //         .catch((error) => {
  //           console.log(error);
  //         });
  //     }
  //   });
  // };

  // const submitActionHandler = async (event) => {
  //   //menjalankan perintah put (update)
  //   event.preventDefault();

  //   await axios.put("http://localhost:3009/produk/{id}" + param.id, {
  //     img: img,
  //     nama: nama,
  //     deskripsi: deskripsi,
  //     harga: harga,
  //   });
  //   Swal.fire("Update Berhasil", "Data berhasil diupdate!", "success")
  //     .then(() => {
  //       history.push("/homeadmin");
  //       window.location.reload();
  //     })
  //     .catch((error) => {
  //       alert("Terjadi kesalahan : " + error);
  //     });
  // };

  return (
    <div className="edit ">
      <div className="my-2 box">
        <Form onSubmit={updateList}>
          <div className="header-update">
            <h1>Update Data</h1>
            <hr />
          </div>
          <div className="name mb-3">
            <Form.Label>
              <strong>Gambar</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                // fileName={img}
                onChange={(e) => setImg(e.target.files[0])}
                type="file"
                accept="image/*"
                className="form-edit"
              />
            </InputGroup>
          </div>

          <div className="name mb-3">
            <Form.Label>
              <strong>Nama</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                value={nama}
                onChange={(e) => setNama(e.target.value)}
                className="form-edit"
              />
            </InputGroup>
          </div>

          <div className="name mb-3">
            <Form.Label>
              <strong>Deskripsi</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                value={deskripsi}
                onChange={(e) => setDeskripsi(e.target.value)}
                className="form-edit"
              />
            </InputGroup>
          </div>

          <div className="name mb-3">
            <Form.Label>
              <strong>Harga</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                value={harga}
                onChange={(e) => setHarga(e.target.value)}
                className="form-edit"
              />
            </InputGroup>
          </div>

          <div className="d-flex justify-content-end align-items-center mt-2">
            <button className="buton btn-save" type="submit">
              Save
            </button>
          </div>
        </Form>
      </div>
    </div>
  );
}

import axios from "axios";
import React, { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import "../style/profil.css";
import Swal from "sweetalert2";
import gambar from "../image/undraw_Profile_details_re_ch9r.png";
import {
  Form,
  FormControl,
  FormLabel,
  InputGroup,
  Modal,
  ModalBody,
  ModalHeader,
  ModalTitle,
} from "react-bootstrap";

export default function ProfilUser() {
  const [show, setShow] = useState(false);
  const [user, setUser] = useState([]);
  const [alamat, setAlamat] = useState("");
  const [email, setEmail] = useState("");
  const [nama, setNama] = useState("");
  const [noTelepon, setNoTelepon] = useState("");
  const [password, setPassword] = useState("");
  const [image, setImage] = useState(null);

  const history = useHistory();

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  useEffect(() => {
    axios
      .get("http://localhost:3009/user/" + localStorage.getItem("userId"))
      .then((res) => {
        const edit = res.data.data;
        setAlamat(edit.alamat);
        setNama(edit.nama);
        setEmail(edit.email);
        setPassword(edit.password);
        setNoTelepon(edit.noTelepon);
        setImage(edit.image);
      });
    getById();
  }, []);

  // const updateProfil = async (e) => {
  //   e.persist();
  //   e.preventDefault();

  //   const formData = new FormData();
  //   formData.append("file", image);
  //   formData.append("email", email);
  //   formData.append("password", password);
  //   formData.append("nama", nama);
  //   formData.append("alamat", alamat);
  //   formData.append("noTelepon", noTelepon);
  //   formData.append("role", "USER");

  //   const result = await axios.put("http://localhost:3009/user", formData, {
  //     headers: { "Content-Type": "multipart/form-data" },
  //   });
  //   console.log(result.data);

  //   // axios.put("http://localhost:3009/user/" + param.id, {
  //   //   alamat: alamat,
  //   //   email: email,
  //   //   nama: nama,
  //   //   noTelepon: noTelepon,
  //   //   password: password,
  //   //   fotoProfil: image
  //   // });
  // };

  const getById = async () => {
    axios
      .get(`http://localhost:3009/user/` + localStorage.getItem("userId"))
      .then((res) => {
        console.log(res.data.data);
        setUser(res.data.data);
      });
  };

  const updateProfil = async (e) => {
    e.persist();
    e.preventDefault();
    const formData = new FormData();
    formData.append("file", image);
    formData.append("email", email);
    formData.append("password", password);
    formData.append("nama", nama);
    formData.append("alamat", alamat);
    formData.append("noTelepon", noTelepon);
    formData.append("role", "USER");
    await axios
      .put(
        "http://localhost:3009/user/" + localStorage.getItem("userId"),
        formData
      )
      .then(() => {
        history.push("/profil");
        Swal.fire("Berhasil", "Data kamu berhasil di update", "success");
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <div className="profiluser">
      <div className="profil">
        <header>
          <h1>Profil Saya</h1>
          <h3>
            Kelola informasi profil Anda untuk mengontrol, melindungi dan
            mengamankan akun
          </h3>
        </header>
        <hr />
        <section className="myprofil">
          <article className="foto">
            <img src={user.fotoProfil} />
            {/* <p>{user.nama}</p> */}
          </article>
          <article className="data-user">
            <div className="ul">
              <ul>
                <li>
                  <i class="fa-solid fa-signature"></i> Nama
                </li>
                <li>
                  <i class="fa-solid fa-envelope"></i> Email
                </li>
                <li>
                  <i class="fa-solid fa-phone"></i> No Telepon
                </li>
                <li>
                  <i class="fa-solid fa-location-dot"></i> Alamat
                </li>
              </ul>
              <ul>
                <li>{user.nama}</li>
                <li>{user.email}</li>
                <li>{user.noTelepon}</li>
                <li>{user.alamat}</li>
              </ul>
            </div>
            <div className="btn-update d-flex justify-content-end align-items-center mt-2">
              <button className="btn-data" onClick={handleShow}>
                Update Data
              </button>
              <Modal show={show} onHide={handleClose} className="modal">
                <ModalHeader closeButton className="header">
                  <ModalTitle>Edit Profil</ModalTitle>
                </ModalHeader>
                <ModalBody>
                  <Form onSubmit={updateProfil}>
                    <div className="mb-3 user-update">
                      <FormLabel>
                        <strong>Nama</strong>
                      </FormLabel>
                      <InputGroup className="d-flex gap-2">
                        <FormControl
                          className="input-user"
                          value={nama}
                          onChange={(e) => setNama(e.target.value)}
                          type="text"
                        />
                      </InputGroup>
                    </div>
                    <div className="mb-3 user-update">
                      <FormLabel>
                        <strong>Foto</strong>
                      </FormLabel>
                      <InputGroup className="d-flex gap-2">
                        <FormControl
                          className="input-user"
                          onChange={(e) => setImage(e.target.files[0])}
                          type="file"
                          accept="image/*"
                        />
                      </InputGroup>
                    </div>
                    <div className="mb-3 user-update">
                      <FormLabel>
                        <strong>Email</strong>
                      </FormLabel>
                      <InputGroup className="d-flex gap-2">
                        <FormControl
                          className="input-user"
                          value={email}
                          type="email"
                          onChange={(e) => setEmail(e.target.value)}
                        />
                      </InputGroup>
                    </div>
                    <div className="mb-3 user-update">
                      <FormLabel>
                        <strong>Password</strong>
                      </FormLabel>
                      <InputGroup className="d-flex gap-2">
                        <FormControl
                          className="input-user"
                          type="password"
                          value={password}
                          onChange={(e) => setPassword(e.target.value)}
                        />
                      </InputGroup>
                    </div>
                    <div className="mb-3 user-update">
                      <FormLabel>
                        <strong>Alamat</strong>
                      </FormLabel>
                      <InputGroup className="d-flex gap-2">
                        <FormControl
                          className="input-user"
                          value={alamat}
                          onChange={(e) => setAlamat(e.target.value)}
                        />
                      </InputGroup>
                    </div>
                    <div className="mb-3 user-update">
                      <FormLabel>
                        <strong>No Telepon</strong>
                      </FormLabel>
                      <InputGroup className="d-flex gap-2">
                        <FormControl
                          className="input-user"
                          value={noTelepon}
                          type="tel"
                          onChange={(e) => setNoTelepon(e.target.value)}
                        />
                      </InputGroup>
                    </div>
                    <button
                      className="mx-1 button-btl btn btn-danger"
                      onClick={handleClose}
                    >
                      Close
                    </button>
                    <button
                      type="submit"
                      className="mx-1 buoton btn btn-primary"
                    >
                      Save
                    </button>
                  </Form>
                </ModalBody>
              </Modal>
            </div>
          </article>
        </section>
      </div>
    </div>
  );
}

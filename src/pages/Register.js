import axios from "axios";
import React, { useState } from "react";
import { Form, InputGroup } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import "../style/register.css";
import login from "../image/undraw_My_password_re_ydq7.png";

export default function Register() {
  // MENYAMBUNGKAN KE DATABASE
  const history = useHistory();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [fotoProfil, setFotoProfil] = useState("");
  const [nama, setNama] = useState("");
  const [alamat, setAlamat] = useState("");
  const [noTelepon, setNoTelepon] = useState(0);

  const register = async (e) => {
    e.preventDefault();
    e.persist();

    const formData = new FormData();
    formData.append("file", fotoProfil);
    formData.append("email", email);
    formData.append("password", password);
    formData.append("nama", nama);
    formData.append("alamat", alamat);
    formData.append("noTelepon", noTelepon);
    formData.append("role", "USER");

    const result = await axios.post("http://localhost:3009/user", formData, {
      headers: { "Content-Type": "multipart/form-data" },
    });
    console.log(result.data);
    Swal.fire({
      icon: "success",
      title: "Berhasil Registrasi",
      showConfirmButton: false,
      timer: 1500,
    });
    history.push("/login");
  };

  // INTEGRASI
  //   const [userRegister, setUserRegister] = useState({
  //   email: "",
  //   password: "",
  //   role: "USER",
  // });

  // const handleOnChange = (e) => {
  //   setUserRegister((currUser) => {
  //     return { ...currUser, [e.target.id]: e.target.value };
  //   });
  // };

  // const register = async (e) => {
  //   e.preventDefault();

  //   try {
  //     const response = await fetch("http://localhost:3009/user", {
  //       method: "POST",
  //       headers: {
  //         "Content-Type": "application/json",
  //       },
  //       body: JSON.stringify(userRegister),
  //     });

  //     if (response.ok) {
  //       alert("Register success");
  //       setTimeout(() => {
  //         history.push("/login");
  //       }, 1250);
  //     }
  //   } catch (err) {
  //     console.log(err);
  //   }
  // };

  // PENGGUNAN AXIOS
  // const [email, setEmail] = useState("");
  // const [password, setPassword] = useState("");

  // const history = useHistory();

  // const addUser = async (e) => {
  //   e.preventDefault();

  //   const data = {
  //     email: email,
  //     password: password,
  //   };

  //   await axios.post("http://localhost:8000/users/", data);
  //   Swal.fire({
  //     icon: "success",
  //     title: "Berhasil Registrasi",
  //     showConfirmButton: false,
  //     timer: 1500,
  //   })
  //     .then(() => {
  //       history.push("/login");
  //       window.location.reload();
  //     })
  //     .catch((error) => {
  //       alert("Terjadi kesalahan " + error);
  //     });
  // };

  return (
    <div className="head-register">
      {/* <h1>LeKafe</h1> */}
      <section className="form-register">
        <article className="img-register">
          <img src={login} alt="img register" />
          <h2>Pesan Makan Mudah Hanya di LeKafe</h2>
          <p>Gabung dan rasakan kemudahan bertransaksi di LeKafe</p>
        </article>
        <article className="register">
          <h1>DAFTAR SEKARANG</h1>
          <p>
            Sudah punya akun? <a href="/login">Masuk</a>
          </p>
          <hr />
          <Form onSubmit={register} method="POST">
            <div className="mb-1">
              <Form.Label>
                <strong>Email</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3 input">
                <Form.Control
                  className="placeholder"
                  placeholder="Masukkan email..."
                  // MENGGUNAKAN SPRING BOOT
                  id="email"
                  // onChange={handleOnChange}
                  // value={userRegister.email}
                  // MENGGUNAKAN JSON SERVER
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  required
                />
              </InputGroup>
            </div>
            <div className="mb-1">
              <Form.Label>
                <strong>Password</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3 input">
                <Form.Control
                  className="placeholder"
                  type="password"
                  id="password"
                  placeholder="Masukkan password..."
                  // MENGGUNAKAN SPRING BOOT
                  // onChange={handleOnChange}
                  // value={userRegister.password}

                  // MENGGUNAKAN JSON SERVER
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  required
                />
              </InputGroup>
            </div>
            <div className="mb-1">
              <Form.Label>
                <strong>Nama</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3 input">
                <Form.Control
                  className="placeholder"
                  type="text"
                  id="nama"
                  placeholder="Masukkan nama..."
                  // MENGGUNAKAN SPRING BOOT
                  // onChange={handleOnChange}
                  // value={userRegister.password}

                  // MENGGUNAKAN JSON SERVER
                  value={nama}
                  onChange={(e) => setNama(e.target.value)}
                  required
                />
              </InputGroup>
            </div>
            <div className="mb-1">
              <Form.Label>
                <strong>Alamat</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3 input">
                <Form.Control
                  className="placeholder"
                  type="text"
                  id="alamat"
                  placeholder="Masukkan alamat lengkap..."
                  // MENGGUNAKAN SPRING BOOT
                  // onChange={handleOnChange}
                  // value={userRegister.password}

                  // MENGGUNAKAN JSON SERVER
                  value={alamat}
                  onChange={(e) => setAlamat(e.target.value)}
                  required
                />
              </InputGroup>
            </div>
            <div className="mb-1">
              <Form.Label>
                <strong>NoTelepon</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3 input">
                <Form.Control
                  className="placeholder"
                  type="tel"
                  id="noTelepon"
                  placeholder="Masukkan nomor telepon..."
                  // MENGGUNAKAN SPRING BOOT
                  // onChange={handleOnChange}
                  // value={userRegister.password}

                  // MENGGUNAKAN JSON SERVER
                  value={noTelepon}
                  onChange={(e) => setNoTelepon(e.target.value)}
                  required
                />
              </InputGroup>
            </div>
            <div className="mb-1">
              <Form.Label>
                <strong>FotoProfile</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3 input">
                <Form.Control
                  className="placeholder"
                  placeholder="Masukkan link gambar..."
                  // onChange={handleOnChange}
                  // value={userRegister.fotoProfil}
                  file={fotoProfil}
                  onChange={(e) => setFotoProfil(e.target.files[0])}
                  type="file"
                  accept="fotoProfil/*"
                  required
                />
              </InputGroup>
            </div>
            <button variant="primary" type="submit" className="mx-1 buton-btn">
              <strong>Daftar</strong>
            </button>
          </Form>
        </article>
      </section>
      <div className="mini-footer">
        <p>
          <i class="fas fa-copyright"></i> Copyright 2022 |{" "}
          <strong>Le<span>Kafe</span> </strong>
        </p>
      </div>
    </div>
  );
}

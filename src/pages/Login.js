import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import axios from "axios";
import { Form, InputGroup } from "react-bootstrap";
import "../style/login.css";
import imglogin from "../image/undraw_Access_account_re_8spm.png";
export default function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const history = useHistory();

  // const addMenu = async (e) => {
  //   e.preventDefault();
  //   e.persist();

  //   const formData = new FormData();
  //   formData.append("file", image);
  //   formData.append("nama", nama);
  //   formData.append("deskripsi", deskripsi);
  //   formData.append("harga", harga);

  //   const result = await axios.post("http://localhost:3009/produk", formData, {
  //     headers: { "Content-Type": "multipart/form-data" },
  //   });
  //   console.log(result.data);
  // };

  const login = async (e) => {
    e.preventDefault();

    try {
      const { data } = await axios.post("http://localhost:3009/user/sign-in", {
        email: email,
        password: password,
      });
      if (data.data.user.role === "ADMIN") {
        Swal.fire({
          icon: "success",
          title: "Login admin berhasil!!",
          showConfirmButton: false,
          timer: 1500,
        });
        localStorage.setItem("userId", data.data.user.id);
        localStorage.setItem("token", data.data.token);
        localStorage.setItem("role", data.data.user.role);
        history.push("/homeAdmin");
      }
      // jika response status 200 (ok)
      // if(status === 200)
      if (data.data.user.role === "USER") {
        Swal.fire({
          icon: "success",
          title: "Login berhasil!!",
          showConfirmButton: false,
          timer: 1500,
        });
        localStorage.setItem("userId", data.data.user.id);
        localStorage.setItem("token", data.data.token);
        localStorage.setItem("role", data.data.user.role);
        history.push("/");
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "Username atau password tidak valid!",
        showConfirmButton: false,
        timer: 1500,
      });
      console.log(error);
    }
  };

  // const login = async (e) => {
  //   e.preventDefault();

  //   try {
  //     const { data } = await axios.post("http://localhost:3009/user/sign-in", {
  //       email: email,
  //       password: password,
  //     });
  //     if (data.data.user.role === "ADMIN") {
  //       Swal.fire({
  //         icon: "success",
  //         title: "Login admin berhasil!!",
  //         showConfirmButton: false,
  //         timer: 1500,
  //       });
  //       localStorage.setItem("userId", data.data.user.id);
  //       localStorage.setItem("token", data.data.token);
  //       localStorage.setItem("role", data.data.user.role);
  //       history.push("/homeAdmin");
  //     }
  //     // jika response status 200 (ok)
  //     // if(status === 200)
  //     if (data.data.user.role === "USER") {
  //       Swal.fire({
  //         icon: "success",
  //         title: "Login berhasil!!",
  //         showConfirmButton: false,
  //         timer: 1500,
  //       });
  //       localStorage.setItem("userId", data.data.user.id);
  //       localStorage.setItem("token", data.data.token);
  //       localStorage.setItem("role", data.data.user.role);
  //       history.push("/");
  //     }
  //   } catch (error) {
  //     Swal.fire({
  //       icon: "error",
  //       title: "Username atau password tidak valid!",
  //       showConfirmButton: false,
  //       timer: 1500,
  //     });
  //     console.log(error);
  //   }
  // };

  // JSON API
  // const login = async (e) => {
  //   e.preventDefault();
  //   axios.get("http://localhost:8000/users").then(({ data }) => {
  //     const user = data.find(
  //       (x) => x.email === email && x.password === password
  //     );
  //     if (user) {
  //       Swal.fire({
  //         icon: "success",
  //         title: "Welcome !!!",
  //         showConfirmButton: false,
  //         timer: 1500,
  //       });
  //       localStorage.setItem("id", user.id);
  //       history.push("/");
  //       setTimeout(() => {
  //         window.location.reload();
  //       }, 1500);
  //     } else {
  //       Swal.fire({
  //         icon: "error",
  //         title: "Email atau Password Tidak Valid!",
  //         showConfirmButton: false,
  //         timer: 1500,
  //       });
  //     }
  //   });
  // };

  return (
    <div className="form">
      <div className="form-login">
        <h1>
          Le<span>Kafe</span>
        </h1>
        <section className="section-login">
          <article className="login">
            <header>
              <h2 className="mb-2">Masuk</h2>
              <h2>
                <a href="/register">Daftar</a>
              </h2>
            </header>
            <hr />
            <Form onSubmit={login} method="POST">
              <div className="mb-3 mt-4">
                <Form.Label>
                  <strong>Email</strong>
                </Form.Label>
                <InputGroup className="d-flex gap-3">
                  <Form.Control
                    className="placeholder-login"
                    placeholder="Masukkan email..."
                    type="text"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                  />
                </InputGroup>
              </div>
              <div className="mb-3">
                <Form.Label>
                  <strong>Password</strong>
                </Form.Label>
                <InputGroup className="d-flex gap-3">
                  <Form.Control
                    className="placeholder-login"
                    placeholder="Masukkan password..."
                    type="password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                  />
                </InputGroup>
              </div>
              <button type="submit" className="buton btn-login">
                Login
              </button>
            </Form>
          </article>
          <article className="img-login">
            <img src={imglogin} alt="image login" />
          </article>
        </section>
      </div>
      <div className="mini-footer">
        <p>
          <i class="fas fa-copyright"></i> Copyright 2022 |{" "}
          <strong>
            Le<span>Kafe</span>
          </strong>
        </p>
      </div>
    </div>
  );
}

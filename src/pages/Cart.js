import axios from "axios";
import React, { useEffect, useState } from "react";
import Swal from "sweetalert2";
import "../style/cart.css";

//jsx = untuk membuat code html di javascript
export default function Cart() {
  const [menu, setMenu] = useState([]);
  const [totalHarga, setTotalPrice] = useState(0);
  // const total = menu.reduce((a, b) => a + b.totalPrice, 0);
  // const [userId, setUserId] = useState(0);

  // GETALL PAGEABLE
  // const getAll = async (page = 0) => {
  //   const resp = await axios
  //     .get(`http://localhost:3009/cart?page=${page}&userId=12`)
  //     .catch((error) => {
  //       alert("Terjadi kesalahan" + error);
  //     });
  //   console.log(resp.data.data.content);
  //   setMenu(resp.data.data.content);
  //   let total = 0;
  //   resp.data.data.content.forEach((item) => {
  //     total += item.totalPrice;
  //   });
  //   setTotalPrice(total);
  // };

  // async await digunakan untuk menunggu program berjalan
  const getAll = async () => {
    const resp = await axios.get(
      `http://localhost:3009/cart?userId=${localStorage.getItem("userId")}`
    );
    console.log(resp);
    setMenu(resp.data.data);
    let total = 0;
    resp.data.data.forEach((item) => {
      total += item.totalPrice;
    });
    setTotalPrice(total);
  };

  useEffect(() => {
    getAll();
  }, []);

  const checkout = async () => {
    await axios.delete(
      `http://localhost:3009/cart?userId=${localStorage.getItem("userId")}`
    );
    Swal.fire({
      title: "Checkout Successfull!",
      text: "Thank you for shopping",
      icon: "success",
      showCancelButton: false,
      timer: 5000,
    });
    getAll();
  };

  const konversionRp = (angka) => {
    return new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR",
      minimumFractionDigits: 0,
    }).format(angka);
  };

  const deleteMenu = async (id) => {
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then(async (result) => {
      if (result.isConfirmed) {
        await axios.delete("http://localhost:3009/cart/" + id);
        Swal.fire("Deleted!", "Your file has been deleted.", "success");
        getAll();
        // window.location.reload();
      }
    });
  };

  return (
    <div>
      {/* <div className="container my-5">
        <div className="table-admin">
          <table className="table table-bordered">
            <thead>
              <tr>
                <th>No</th>
                <th>Gambar</th>
                <th>Nama</th>
                <th>Deskripsi</th>
                <th className="harga">Harga</th>
                <th className="aksi">Aksi</th>
              </tr>
            </thead>
            <tbody>
              {menu.map((daftar, index) => (
                <tr key={daftar.id}>
                  <td>{index + 1}</td>
                  <td>
                    <img src={daftar.gambar} />
                  </td>
                  <td>{daftar.nama}</td>
                  <td>{daftar.deskripsi}</td>
                  <td>Rp {daftar.harga}</td>
                  <td>
                    <button
                      className="mx-1 tombol delete"
                      onClick={() => deleteMenu(daftar.id)}
                    >
                      Hapus
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div> */}

      {localStorage.getItem("userId") !== null ? (
        <div className="container my-5">
          <div className="table-admin">
            <table className="table table-bordered">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Gambar</th>
                  <th>Nama</th>
                  <th>Jumlah</th>
                  <th>Harga</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                {menu.map((daftar, index) => (
                  <tr key={daftar.id} className="cart-table">
                    <td>{index + 1}</td>
                    <td className="gambar">
                      <img src={daftar.produk.img} />
                    </td>
                    <td className="nama">{daftar.produk.nama}</td>
                    <td className="jumlah-produk">{daftar.quantity}</td>
                    <td className="harga">{konversionRp(daftar.totalPrice)}</td>
                    <td className="aksi">
                      <button
                        className="mx-1 tombol delete"
                        onClick={() => deleteMenu(daftar.id)}
                      >
                        Hapus
                      </button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      ) : (
        <></>
      )}
      <strong className="total">
        Total Harga : {konversionRp(totalHarga)}
      </strong>
      <div className="checkout">
        <button onClick={() => checkout()}>CheckOut</button>
      </div>
    </div>
  );
}

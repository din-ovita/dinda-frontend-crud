import axios from "axios";
import React, { useEffect, useState } from "react";
import Swal from "sweetalert2";
import "../style/homeadmin.css";
import ReactPaginate from "react-paginate";

export default function HomeAdmin() {
  const [menu, setMenu] = useState([]);
  const [pages, setPages] = useState(0);
  const [nama, setNama] = useState("");

  // PENGGUNAAN DATABASE BE
  const getAll = async (idx) => {
    await axios
      // .get("http://localhost:3009/produk/all-produk")
      .get(`http://localhost:3009/produk/all-produk?nama=${nama}&page=${idx}`)
      // ?search_query=${keyword}&page=${page}&limit=${limit}
      .then((res) => {
        console.log(res.data.data.content);
        setMenu(res.data.data.content);
        setPages(res.data.data.totalPages);
      })
      .catch((error) => {
        alert("Terjadi kesalahan " + error);
      });
  };

  // const searchData = (e) => {

  // PENGGUNAAN DATABASE JSON SERVER
  // const getAll = () => {
  //   axios
  //     .get("http://localhost:8000/daftarMenu")
  //     .then((res) => {
  //       setMenu(res.data);
  //     })
  //     .catch((error) => {
  //       alert("Terjadi kesalahan" + error);
  //     });
  // };

  // const changePage = (data) => {
  //   console.log(data.selected);
  //   setPage(data.selected);
  //   getAll();
  // }

  useEffect(() => {
    getAll(0);
  }, []);

  const deleteMenu = async (id) => {
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete("http://localhost:3009/produk/" + id);
        Swal.fire({
          title: "Deleted!",
          text: "Your file has been deleted.",
          icon: "success",
          timer: 5000,
        });
      }
    });
    getAll(0);
  };

  return (
    <div className="homeadmin">
      <div className="container data-product my-1">
        <div className="columns">
          <div className="column is-centered">
            <form class="d-flex mb-3" role="search">
              <input
                class="form-control me-2"
                type="search"
                value={nama}
                onChange={(e) => setNama(e.target.value)}
                placeholder="Search"
                aria-label="Search"
              />
              <button
                class="btn btn-outline-success"
                type="button"
                onClick={() => getAll(0)}
              >
                Search
              </button>
            </form>
            <div className="table-admin">
              <table class="table">
                <thead>
                  <tr>
                    <th scope="col">No</th>
                    <th scope="col">Gambar</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Deskripsi</th>
                    <th scope="col">Harga</th>
                    <th scope="col">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  {menu.map((daftar, index) => (
                    <tr key={daftar.id} className="data-produk">
                      <th scope="row">{index + 1}</th>
                      <td className="gambar-produk">
                        <img src={daftar.img} />
                      </td>
                      <td className="nama-produk">{daftar.nama}</td>
                      <td className="deskripsi-produk">{daftar.deskripsi}</td>
                      <td className="harga-produk">Rp {daftar.harga}</td>
                      <td className="aksi-admin">
                        <button
                          className="mx-1 tombol delete"
                          onClick={() => deleteMenu(daftar.id)}
                        >
                          <i class="fa-solid fa-trash-can"></i>
                        </button>{" "}
                        <a href={"/edit/" + daftar.id}>
                          <button className="mx-1 tombol ubah"><i class="fa-solid fa-pen-to-square"></i></button>
                        </a>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
            {menu.length == 0 ? (
              <h5 align="center" className="my-5">Produk tidak tersedia</h5>
            ) : (
              <nav>
                <ReactPaginate
                  previousLabel={"< Prev"}
                  nextLabel={"Next >"}
                  pageCount={pages}
                  breakLabel={"..."}
                  onPageChange={(e) => getAll(e.selected)}
                  containerClassName={"pagination justify-content-center"}
                  pageClassName={"page-item"}
                  pageLinkClassName={"page-link"}
                  previousLinkClassName={"page-link"}
                  nextClassName={"page-item"}
                  nextLinkClassName={"page-link"}
                  activeClassName={"active"}
                />
              </nav>
            )}
            {/* <div className="table-admin">
              <table className="table table-bordered">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Gambar</th>
                    <th>Nama</th>
                    <th>Deskripsi</th>
                    <th>Harga</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  {menu.map((daftar, index) => (
                    <tr key={daftar.id}>
                      <td>{index + 1}</td>
                      <td>
                        <img src={daftar.img} />
                      </td>
                      <td>{daftar.nama}</td>
                      <td className="deskripsi">{daftar.deskripsi}</td>
                      <td className="harga">Rp {daftar.harga}</td>
                      <td className="aksi">
                        <button
                          className="mx-1 tombol delete"
                          onClick={() => deleteMenu(daftar.id)}
                        >
                          Hapus
                        </button>{" "}
                        <a href={"/edit/" + daftar.id}>
                          <button className="mx-1 tombol ubah">Ubah</button>
                        </a>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
              {menu.length == 0 ? (
                <h5 align="center">Produk tidak tersedia</h5>
              ) : (
                <nav>
                  <ReactPaginate
                    previousLabel={"< Prev"}
                    nextLabel={"Next >"}
                    pageCount={pages}
                    breakLabel={"..."}
                    onPageChange={(e) => getAll(e.selected)}
                    containerClassName={"pagination justify-content-center"}
                    pageClassName={"page-item"}
                    pageLinkClassName={"page-link"}
                    previousLinkClassName={"page-link"}
                    nextClassName={"page-item"}
                    nextLinkClassName={"page-link"}
                    activeClassName={"active"}
                  />
                </nav>
              )} */}
            {/* == tipe data tidak harus sama */}
            {/* === tipe data harus sama */}
          </div>
        </div>
      </div>
    </div>
    // </div>
  );
}

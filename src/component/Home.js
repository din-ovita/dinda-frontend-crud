import axios from "axios";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import promo1 from "../image/promo3.jpeg";
import promo2 from "../image/promo2.jpg";
import promo3 from "../image/promo1.jpg";
import img1 from "../image/pngegg (14).png";
import img2 from "../image/pngegg (8).png";
import img3 from "../image/pngegg (10).png";
import "../style/home.css";
import ReactPaginate from "react-paginate";

export default function Home() {
  const history = useHistory();
  const [pages, setPages] = useState(0);
  const [menu, setMenu] = useState([]);
  const [nama, setNama] = useState("");

  //DATA BE
  const getAll = async (idx) => {
    const resp = await axios
      .get(`http://localhost:3009/produk/all-produk?nama=${nama}&page=` + idx)
      .catch((error) => {
        alert("Terjadi kesalahan " + error);
      });
    // setMenu(resp.data.data.content)
    setPages(resp.data.data.totalPages);
    setMenu(
      resp.data.data.content.map((daftar) => ({
        ...daftar,
        qty: 1,
      }))
    );
  };

  // const addToCart = async (daftar) => {
  //   await axios.post("http://localhost:3009/cart/", daftar);
  //   Swal.fire({
  //     icon: "success",
  //     title: "Berhasil dimasukkan ke Cart",
  //     showConfirmButton: false,
  //     timer: 1500,
  //   })
  //     .then(() => {
  //       window.location.reload();
  //       history.push("/cart");
  //     })
  //     .catch((error) => {
  //       alert("Terjadi kesalahan " + error);
  //     });
  // };

  const addToCart = async (daftar) => {
    console.log(daftar);
    const user = localStorage.getItem("userId");
    await axios.post(`http://localhost:3009/cart/`, {
      userId: user,
      produkId: daftar.id,
      quantity: daftar.qty,
    });
    Swal.fire({
      icon: "success",
      title: "Berhasil dimasukkan ke Cart",
      showConfirmButton: false,
      timer: 1500,
    })
      .then(() => {
        history.push("/cart");
      })
      .catch((error) => {
        alert("Terjadi kesalahan " + error);
      });
  };

  const plusQuantity = (idx) => {
    const produk = [...menu];
    console.log(produk);
    // condition
    // if (produk[idx].qty <= 45) {
    //   setMenu(produk);
    // }
    if (produk[idx].qty >= 45) return;
    produk[idx].qty++;
    setMenu(produk);
  };

  const minQuantity = (idx) => {
    const produk = [...menu];
    console.log(produk);
    // condition
    // if (produk[idx].qty > 0) {
    //   setMenu(produk);
    // }
    if (produk[idx].qty <= 1) return;
    produk[idx].qty--;
    setMenu(produk);
  };

  //PENGGUNAAN JSONSERVER
  // const getAll = () => {
  //   axios
  //     .get("http://localhost:8000/daftarMenu")
  //     .then((res) => {
  //       setMenu(res.data);
  //     })
  //     .catch((error) => {
  //       alert("Terjadi kesalahan" + error);
  //     });
  // };

  useEffect(() => {
    getAll(0);
  }, []);

  return (
    <div className="home">
      <div className="promo-kafe">
        <div id="carouselExampleIndicators" class="carousel slide">
          <div class="carousel-indicators">
            <button
              type="button"
              data-bs-target="#carouselExampleIndicators"
              data-bs-slide-to="0"
              class="active"
              aria-current="true"
              aria-label="Slide 1"
            ></button>
            <button
              type="button"
              data-bs-target="#carouselExampleIndicators"
              data-bs-slide-to="1"
              aria-label="Slide 2"
            ></button>
            <button
              type="button"
              data-bs-target="#carouselExampleIndicators"
              data-bs-slide-to="2"
              aria-label="Slide 3"
            ></button>
          </div>
          <div class="carousel-inner">
            <div class="carousel-item active">
              <section>
                <article className="note">
                  <small>Menu Istimewa Kami</small>
                  <strong>Cappucino</strong>
                  <p>
                    Secangkir kopi cappuccino memiliki perbandingan espresso,
                    steamed milk dan busa yang sama. Rasa kopi cappuccino
                    umumnya sangat rich, walaupun sudah dicampur susu, rasa
                    kopinya tetap kuat.
                  </p>
                  <p>
                    <span>
                      Diskon 10% <i class="fa-solid fa-tags"></i>
                    </span>
                    khusus pengguna baru
                  </p>
                </article>
                <article className="img-carousel">
                  <img src={img1} class="d-blok w-100" alt="..." />
                </article>
              </section>
            </div>
            <div class="carousel-item">
              <section>
                <article className="note">
                  <small>Menu Istimewa Kami</small>
                  <strong>Waffle</strong>
                  <p>
                    Waffle memiliki tekstur empuk dan renyah. Dibuat dari adonan
                    tepung terigu, mentega, susu dan telur. Disajikan dengan
                    berbagai topping. Mulai dari buah-buahan hingga aneka rasa
                    es krim.
                  </p>
                  <p>
                    <span>
                      Diskon 8% <i class="fa-solid fa-tags"></i>
                    </span>
                    khusus pengguna baru
                  </p>
                </article>
                <article className="img-carousel">
                  <img src={img2} class="d-blok" alt="..." />
                </article>
              </section>{" "}
            </div>
            <div class="carousel-item">
              <section>
                <article className="note">
                  <small>Menu Istimewa Kami</small>
                  <strong>Pizza</strong>
                  <p>
                    Makanan siap saji yang praktis dan mengenyangkan. Isi
                    topping pizza ini berupa jamur, paprika hijau, paprika
                    merah, onion/bawang bombay, daging asap, dan keju mozarella.
                    Jadi tidak hanya kaya akan rasa, namun juga sehat untuk
                    disantap.
                  </p>
                  <p>
                    <span>
                      Diskon 5% <i class="fa-solid fa-tags"></i>
                    </span>
                    khusus pengguna baru
                  </p>
                </article>
                <article className="img-carousel">
                  <img src={img3} class="d-blok" alt="..." />
                </article>
              </section>{" "}
            </div>
          </div>
          <button
            class="carousel-control-prev"
            type="button"
            data-bs-target="#carouselExampleIndicators"
            data-bs-slide="prev"
          >
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
          </button>
          <button
            class="carousel-control-next"
            type="button"
            data-bs-target="#carouselExampleIndicators"
            data-bs-slide="next"
          >
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
          </button>
        </div>
      </div>

      <form class="container d-flex mb-3 mt-3" role="search">
        <input
          class="form-control me-2"
          type="search"
          value={nama}
          onChange={(e) => setNama(e.target.value)}
          placeholder="Search"
          aria-label="Search"
        />
        <button
          class="btn btn-outline-success"
          type="button"
          onClick={() => getAll(0)}
        >
          Search
        </button>
      </form>

      <div className="produk flex-wrap gap-3">
        {menu.map((daftar, idx) => (
          <div key={daftar.id} className="card">
            <div className="img">
              <img src={daftar.img} className="card-img-top" />
            </div>
            <div className="card-body">
              <h3 class="card-title">{daftar.nama}</h3>
              <h5 class="card-text">{daftar.deskripsi}</h5>
              <div className="jumlah">
                <div className="qty">
                  <button
                    className={` ${daftar.qty == 1 ? "gray" : "green"}`}
                    onClick={() => minQuantity(idx)}
                  >
                    -
                  </button>
                  <span>{daftar.qty}</span>
                  <button
                    onClick={() => plusQuantity(idx)}
                    className={` ${daftar.qty == 45 ? "gray" : "green"}`}
                  >
                    +
                  </button>
                </div>
                <h4>Rp {daftar.harga}</h4>
              </div>
              {localStorage.getItem("userId") !== null ? (
                <div className="buy">
                  <a onClick={() => addToCart(daftar)}>Buy</a>
                </div>
              ) : (
                <></>
              )}
            </div>
          </div>
        ))}
      </div>

      {/* <div class="card" style="width: 18rem;">
        {menu.map((daftar, idx) => (
          <div key={daftar.id} className="card-produk">
            <img src={daftar.img} class="card-img-top" alt="Gambar produk" />
            <div class="card-body">
              <h5 class="card-title">{daftar.nama}</h5>
              <h4 class="card-text">{daftar.deskripsi}</h4>
              <div className="jumlah">
                <div className="qty">
                  <button
                    className={`min ${daftar.qty == 1 ? "gray" : "green"}`}
                    onClick={() => minQuantity(idx)}
                  >
                    -
                  </button>
                  {daftar.qty}
                  <button
                    onClick={() => plusQuantity(idx)}
                    className={`plus ${daftar.qty == 45 ? "gray" : "green"}`}
                  >
                    +
                  </button>
                </div>
                <h4>Rp {daftar.harga}</h4>
              </div>
              <a href="#" class="btn btn-primary"></a>
            </div>
          </div>
        ))}
      </div> */}

      {/* {daftar.qty == 1 ? (
                    <button
                      className="min gray"
                      onClick={() => minQuantity(idx)}
                    >
                      -
                    </button>
                  ) : (
                    <button
                      className="min green"
                      onClick={() => minQuantity(idx)}
                    >
                      -
                    </button>
                  )} */}

      {/* {daftar.qty == 45 ? (
                    <button
                      onClick={() => plusQuantity(idx)}
                      className="plus gray"
                    >
                      +
                    </button>
                  ) : (
                    <button
                      onClick={() => plusQuantity(idx)}
                      className="plus green"
                    >
                      +
                    </button>
                  )} */}
      {/* <input type="text" value={daftar.qty} /> */}

      {menu.length == 0 ? (
        <h5 align="center" className="mb-4">
          Produk tidak tersedia
        </h5>
      ) : (
        <nav>
          <ReactPaginate
            previousLabel={"< Prev"}
            nextLabel={"Next >"}
            pageCount={pages}
            breakLabel={"..."}
            onPageChange={(e) => getAll(e.selected)}
            containerClassName={"pagination justify-content-center"}
            pageClassName={"page-item"}
            pageLinkClassName={"page-link"}
            previousLinkClassName={"page-link"}
            nextClassName={"page-item"}
            nextLinkClassName={"page-link"}
            activeClassName={"active"}
          />
        </nav>
        // nav pagenation
      )}
    </div>
  );
}
